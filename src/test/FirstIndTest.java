package test;

import FirstInd.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FirstIndTest {
    @Test
    public void alphabetNotOneSymbol() throws FileNotFoundException, AutomataException {
        FileReader fileReader = new FileReader("resources/test/alphabetNotOneSymbol.txt");
        Assertions.assertThrows(AutomataProcessingException.class, () -> {
            Automata result = new AutomataProcessor().processAndCreateAutomata(fileReader);
        }, "Wrong alphabet definition: alphabet character myst be one symbol long");
    }
    @Test
    public void incorrectStateTransitionsQuantity() throws FileNotFoundException, AutomataException {
        FileReader fileReader = new FileReader("resources/test/incorrectStateTransitionsQuantity.txt");
        Assertions.assertThrows(AutomataProcessingException.class, () -> {
            Automata result = new AutomataProcessor().processAndCreateAutomata(fileReader);
        }, "Wrong state definition. There should be as many transitions as characters of the alphabet." + "State name " + "B");
    }

    @Test
    public void incorrectAcceptingFlag() throws FileNotFoundException, AutomataException {
        FileReader fileReader = new FileReader("resources/test/incorrectAcceptingFlag.txt");
        Assertions.assertThrows(AutomataProcessingException.class, () -> {
            Automata result = new AutomataProcessor().processAndCreateAutomata(fileReader);
        }, "Wrong state definition. The accepting flag must be 0 or 1. Actual: '" + "3" + "' State name " + "A");
    }
    @Test
    public void incorrectAutomata() throws FileNotFoundException, AutomataProcessingException {
        FileReader fileReader = new FileReader("resources/test/incorrectAutomata.txt");
        Assertions.assertThrows(AutomataException.class, () -> {
            Automata result = new AutomataProcessor().processAndCreateAutomata(fileReader);
        }, "Undefined state '" + "C" + "' in '" + "A" + "' state definition");
    }
    @Test
    public void correctAutomata() throws FileNotFoundException, InvalidStateNameException, AutomataProcessingException, AutomataException {
        FileReader fileReader = new FileReader("resources/test/correctAutomata.txt");
        Automata automata;
        automata = new AutomataProcessor().processAndCreateAutomata(fileReader);
        List<String> expectedAlphabet = new ArrayList<String>();
        expectedAlphabet.add("0");
        expectedAlphabet.add("1");
        expectedAlphabet.add("2");
        Assertions.assertEquals(expectedAlphabet, automata.getAlphabet());
        Map<String, String> aTrans = new HashMap<>();
        aTrans.put("0","A");
        aTrans.put("1","A");
        aTrans.put("2","B");
        Map<String, String> bTrans = new HashMap<>();
        bTrans.put("0","A");
        bTrans.put("1","B");
        bTrans.put("2","f");
        Map<String, String> fTrans = new HashMap<>();
        fTrans.put("0","A");
        fTrans.put("1","A");
        fTrans.put("2","A");
        State A = new State("A", aTrans,false);
        State B = new State("B", bTrans,false);
        State f = new State("f", fTrans,true);
        Assertions.assertEquals(A, automata.getTransitionMatrix().get("A"));
        Assertions.assertEquals(B, automata.getTransitionMatrix().get("B"));
        Assertions.assertEquals(f, automata.getTransitionMatrix().get("f"));
        Assertions.assertEquals(A, automata.getStateByName(automata.getBeginState()));
    }

    @Test
    public void incorrectStateName() throws FileNotFoundException, AutomataProcessingException, AutomataException {
        FileReader fileReader = new FileReader("resources/test/Automata.txt");
        Automata automata = new AutomataProcessor().processAndCreateAutomata(fileReader);
        Assertions.assertThrows(InvalidStateNameException.class, () -> {
            automata.getStateByName("");
        }, "Empty state name");
    }

    @Test
    public void transitionsTest() throws FileNotFoundException, AutomataProcessingException, AutomataException, InvalidStateNameException {
        FileReader fileReader = new FileReader("resources/test/Automata.txt");
        Automata automata = new AutomataProcessor().processAndCreateAutomata(fileReader);
        Map<String, String> aTrans = new HashMap<>();
        aTrans.put("0","B");
        aTrans.put("1","C");
        Map<String, String> bTrans = new HashMap<>();
        bTrans.put("0","A");
        bTrans.put("1","C");
        State A = new State("A", aTrans, false);
        State B = new State("B", bTrans, false);
        Assertions.assertThrows(AutomataException.class, () -> {
            State chainState = automata.getTransitionState("A", "3");
        }, "Symbol is not defined");
        Assertions.assertThrows(AutomataException.class, () -> {
            State chainState = automata.getTransitionState("D", "3");
        }, "State is not defined");
        State chainState = automata.getTransitionState("A", "");
        Assertions.assertEquals(chainState, A);
        chainState = automata.getTransitionState("A", "0");
        Assertions.assertEquals(chainState, B);
    }

    @Test
    public void stringAccepting() throws FileNotFoundException, AutomataProcessingException, AutomataException, InvalidStateNameException {
        FileReader fileReader = new FileReader("resources/test/Automata.txt");
        Automata automata = new AutomataProcessor().processAndCreateAutomata(fileReader);
        Assertions.assertThrows(AutomataException.class, () -> {
            boolean result = automata.isAcceptingString("02", false);
        }, "String has undefined alphabet character '" + 2 + "' at " + 1 + "th position");
        boolean accept = automata.isAcceptingString("01", false);
        Assertions.assertTrue(accept);
        boolean decline = automata.isAcceptingString("", false);
        Assertions.assertFalse(decline);
    }
}
