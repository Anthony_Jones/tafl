/*
 * Copyright © 2019 Anton Ivaniv. All rights reserved.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package FirstInd;

import java.util.Map;
import java.util.Objects;

/**
 * Класс представляющий состояние какого-то конечного распознавателя {@link Automata}
 * @author anthonyj
 * @version 1.0
 */
public class State {
    private Map<String, String> transitions; /// Строка переходов состояние. Ключом яляет имя состояние {@field stateName}
    private boolean isAcceptingState; /// Флаг показывающий ялвяется ли это состояние допускающим
    private String stateName; /// Имя сотояния

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public State(String stateName, Map<String, String> transitions, boolean isAcceptingState) {
        this.transitions = transitions;
        this.isAcceptingState = isAcceptingState;
        this.stateName = stateName;
    }

    public Map<String, String> getTransitions() {
        return transitions;
    }

    public void setTransitions(Map<String, String> transitions) {
        this.transitions = transitions;
    }

    public boolean isAcceptingState() {
        return isAcceptingState;
    }

    public void setAcceptingState(boolean acceptingState) {
        isAcceptingState = acceptingState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof State)) return false;
        State state = (State) o;
        return isAcceptingState() == state.isAcceptingState() &&
                getTransitions().equals(state.getTransitions()) &&
                getStateName().equals(state.getStateName());
    }

    @Override
    public String toString() {
        StringBuilder trans = new StringBuilder();
        trans.append('|');
        for (String i:
             transitions.values()) {
            trans.append(i).append(" ");
        }
        trans.deleteCharAt(trans.length() - 1);
        trans.append("| ");
        return stateName + " " +
                trans + ((isAcceptingState) ? 1: 0) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTransitions(), isAcceptingState(), getStateName());
    }
}
