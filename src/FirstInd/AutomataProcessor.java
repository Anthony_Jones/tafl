/*
 * Copyright © 2019 Anton Ivaniv. All rights reserved.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package FirstInd;

import FirstInd.State;
import java.io.FileReader;
import java.util.*;

/**
 * Класс для обработки текстового файла задающего автомат
 * @author anthonyj
 * @version 1.0
 */
public class AutomataProcessor {
    /**
     * Проверяет правильность текстового файла задающего автомат и правильность задания автомата.
     * Делегирует создание автомата и анализ его на неопределённые состояния конструктору <code>Automata</code>
     * @param automata Текстовый файл с автоматом.
     * @return Корректный автомат  <code>Automata</code>
     * @throws AutomataProcessingException Если текстовый файл задающий автомат не корректен
     * @throws AutomataException Если автомат заданный файлом не корректен
     * @see Automata
     */
    public Automata processAndCreateAutomata(FileReader automata) throws AutomataProcessingException, AutomataException {
        Scanner scan = new Scanner(automata);
        Map<String, State> transitionMatrix = new HashMap<String, State>();
        List<String> alphabet = new ArrayList<>();
        String beginState = "";
        String line = "";
        if (scan.hasNextLine()) { // Строка алфавита
            line = scan.nextLine();
            String[] alphabetTemp = line.trim().split("\\s+");
            for (String i: alphabetTemp) {
                if(i.length() > 1) {
                    throw new AutomataProcessingException("Wrong alphabet definition: alphabet character myst be one symbol long");
                } else {
                    alphabet.add(i);
                }
            }
        }
        boolean firstState = true;
        while(scan.hasNextLine()) {
            line = scan.nextLine().trim();
            if(line.isEmpty()) {
                continue;
            }
            StringBuilder stateNameKey = new StringBuilder();
            StringBuilder stateNameValue = new StringBuilder();

            String[] statesTemp = line.trim().split("\\s+");
            if(statesTemp.length != alphabet.size() + 2) {
                throw new AutomataProcessingException("Wrong state definition. There should be as many transitions as characters of the alphabet." + "State name " +statesTemp[0]);
            }
            if(firstState) {
                beginState = statesTemp[0];
                firstState = false;
            }
            Map<String, String> stateTransitions = new HashMap<>();
            String stateName = statesTemp[0];
            for (int i = 1; i < statesTemp.length - 1; i++) {
                stateTransitions.put(alphabet.get(i - 1), statesTemp[i]);
            }
            if(!statesTemp[statesTemp.length - 1].equals("0") && !statesTemp[statesTemp.length - 1].equals("1")) {
                throw new AutomataProcessingException("Wrong state definition. The accepting flag must be 0 or 1. Actual: '" + statesTemp[statesTemp.length - 1] + "' State name " +statesTemp[0]);
            }
            boolean isAcceptingState = statesTemp[statesTemp.length - 1].equals("1");
            State state = new State(stateName, stateTransitions, isAcceptingState);
            transitionMatrix.put(stateName, state);
        }
        Automata automata1 = new Automata(transitionMatrix, alphabet, beginState);
        return automata1;
    }
}
