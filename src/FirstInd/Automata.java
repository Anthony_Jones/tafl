/*
 * Copyright © 2019 Anton Ivaniv. All rights reserved.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package FirstInd;


import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Класс математического объекта конечный дитерминированный распознаватель(Автомат)
 * Доступный функционал:
 * Проверить является ли цепочка допускающей или нет
 * @throws AutomataException,InvalidStateNameException
 * @author anthonyj
 * @version 1.0
 * todo English DOC
 */
public class Automata {
    protected Map<String, State> transitionMatrix;
    protected List<String> alphabet;
    protected String beginStateName;

    public String getBeginState() {
        return beginStateName;
    }

    public void setBeginState(State beginState) {
        transitionMatrix.remove(beginState.getStateName(), beginState);
        this.beginStateName = beginState.getStateName();
    }

    /**
     * Стандартный используемый конструктор. Анализирует автомат на неопределённые состояния.
     * @param transitionMatrix Матрица переходов Автомата
     * @param alphabet Алфавит входных цепочек(Один элемент алфавита - один символ)
     * @param beginStateName Имя начального состояния
     * @throws AutomataException Синтаксическая ошибка в определении автомата - не определённое состояние
     */
    public Automata(Map<String, State> transitionMatrix, List<String> alphabet, String beginStateName) throws AutomataException {
        Set<String> states = transitionMatrix.keySet();
        for (State state :
                transitionMatrix.values()) {
            for (String stateName:
                 state.getTransitions().values()) {
                if(!states.contains(stateName)) {
                    throw new AutomataException("Undefined state '" + stateName + "' in '" + state.getStateName() + "' state definition");
                }
            }
        }
        this.transitionMatrix = transitionMatrix;
        this.alphabet = alphabet;
        if (!beginStateName.isEmpty()) {
            this.beginStateName = beginStateName;
        }

    }

    public Map<String, State> getTransitionMatrix() {
        return transitionMatrix;
    }

    public void setTransitionMatrix(Map<String, State> transitionMatrix) {
        this.transitionMatrix = transitionMatrix;
    }

    public List<String> getAlphabet() {
        return alphabet;
    }

    public void setAlphabet(List<String> alphabet) {
        this.alphabet = alphabet;
    }

    /**
     * Получить состояние из матрицы переходов по имени
     * @param stateName Имя состояния
     * @return Состояние с именем stateName
     * @throws InvalidStateNameException Такого состояния нет, или пустая строка
     */
    public State getStateByName(String stateName) throws InvalidStateNameException {
        if (!stateName.isEmpty()) {
            if (transitionMatrix.containsKey(stateName)) {
                return transitionMatrix.get(stateName);
            } else {
                throw new InvalidStateNameException("State is not defined");
            }
        } else {
            throw new InvalidStateNameException("Empty state name");
        }
    }

    /**
     * По имени состояния A и букве алфавита получить получить состояние B из таблицы переходов
     * @param stateName Имя состояние A
     * @param alphabetCharacter Буква алфавита по которому производиться переход в состояние B
     * @return Состояние B
     * @throws AutomataException Если не существует такого символа в алфавите
     * @throws InvalidStateNameException Если такого состояние не существует или оно пустое
     */
    public State getTransitionState(String stateName, String alphabetCharacter) throws AutomataException, InvalidStateNameException {
        State state;
        try {
            state = this.getStateByName(stateName);
        } catch (InvalidStateNameException invalidStateNameException) {
            invalidStateNameException.printStackTrace();
            throw new AutomataException(invalidStateNameException.getMessage());
        }
        if (alphabetCharacter.isEmpty()) {
            return state;
        } else {
            if (!alphabet.contains(alphabetCharacter)) {
                throw new AutomataException("Symbol is not defined");
            } else {
                return this.getStateByName(state.getTransitions().get(alphabetCharacter));
            }
        }
    }

    /**
     * Принимает на вход цепочку символов. Используя таблицу переходов переходит из начального состояния в конечное.
     * @param string Цепочка символов
     * @param showPath Флаг показывать ли путь цепочки.
     * @return Является ли конечное состояние допускающим или нет
     * @throws AutomataException Если во входной цепочке используется символ не входящий во входной алфавит автомата
     * @throws InvalidStateNameException Если произошла дичь и состояние в переходе есть, а в таблице нет. Кто-то что-то испортил!
     */
    public boolean isAcceptingString(String string, boolean showPath) throws AutomataException, InvalidStateNameException {
        String path = this.beginStateName;
        State state = this.getStateByName(this.beginStateName);
        int i = 0;
        for (Character c : string.toCharArray()) {

            if (alphabet.contains(c.toString())) {
                state = getTransitionState(state.getStateName(), c.toString());
                path += " => " + state.getStateName();
            } else {
                throw new AutomataException("String has undefined alphabet character '"+c+"' at position" + i);
            }
            ++i;
        }
        if (showPath) {
            System.out.println(path);
        }
        return state.isAcceptingState();
    }

    @Override
    public String toString() {
        StringBuilder alphabet = new StringBuilder();
        alphabet.append("   ");
        for (String i :
                this.alphabet) {
            alphabet.append(i).append(" ");
        }
        String automatas = " ";
        try {
            automatas = this.getStateByName(this.beginStateName).toString();
        } catch (InvalidStateNameException e) {
            e.printStackTrace();
        }
        StringBuilder stringBuilder = new StringBuilder(automatas);
        stringBuilder.append("\n");
        for (State i :
                transitionMatrix.values()) {
            if (!i.getStateName().equals(this.beginStateName)) {
                stringBuilder.append(i.toString()).append("\n");
            }
        }
        return alphabet + "\n" +stringBuilder.toString();
    }

    public Automata() {
    }
}
