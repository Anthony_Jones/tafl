/*
 * Copyright © 2019 Anton Ivaniv. All rights reserved.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package FirstInd;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        FileReader fileReader = new FileReader("resources/FirstInd/avtomat.txt");
        AutomataProcessor automataProcessor = new AutomataProcessor();
        Automata automata;
        try {
            automata = automataProcessor.processAndCreateAutomata(fileReader);
        } catch (AutomataProcessingException | AutomataException e) {
            System.out.println("=============================");
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.out.println("=============================");
            return;
        }
        FileReader fileReader1 = new FileReader("resources/FirstInd/cep.txt");
        Scanner scan = new Scanner(fileReader1);
        boolean isAccepingString = false;
        if(!scan.hasNextLine()) {
            try {
                isAccepingString = automata.isAcceptingString("", false);
            } catch (AutomataException | InvalidStateNameException e) {
            System.out.println("=============================");
            System.out.println(e);
            e.printStackTrace();
            System.out.println("=============================");
            return;
            }
        } else {
            String string = scan.nextLine().trim();
            if(isCorrectString(string, automata.getAlphabet())) {
                try {
                    isAccepingString = automata.isAcceptingString(string, true);
                } catch (AutomataException | InvalidStateNameException e) {
                    System.out.println("=============================");
                    System.out.println(e);
                    e.printStackTrace();
                    System.out.println("=============================");
                    return;
                }
            } else {
                System.out.println("===========================");
                System.out.println("Please enter a valid string");
                System.out.println("===========================");
                return;
            }
        }
        if(isAccepingString) {
            System.out.println("===================");
            System.out.println("Is Accepting String");
            System.out.println("===================");
        } else {
            System.out.println("=======================");
            System.out.println("Is not Accepting String");
            System.out.println("=======================");
        }
        System.out.println(automata.toString());
    }

    public static boolean isCorrectString(String string, List<String> alphabet) {
        for (Character c :
                string.toCharArray()) {
            if (!alphabet.contains(c.toString())) {
                return false;
            }
        }
        return true;
    }
}
